<?php

// language
$lang="FR";//select(FR,EN) Language first is default

// option for labels
$shadow=1;//checkbox(1) Shadow or not shadow for the labels
$roundborder=4;//integer(4) size for the roundborder
$labelwidth=164;//integer(164) px
$variablewidth=1;//checkbox() to fix or not the width of all labels to $labelwidth
$labelheight=16;//integer(16) px
$labelfontsize=14;//integer(14) px
$upcasefirstname=1;//checkbox(1) 
$labelbackgroundcolor="lightgreen";//text() 
$labelpencolor="black";//text() 

// category on label
$categories_sum=0;//checkbox() 
$category_in_label=0;//checkbox() 

// disposition
$lateral_links=1;//checkbox(1) with 0, rake for all, except blocks (-r and -l are interpreted as --)
$block_mode=1;//checkbox(1) with 0 if all son haven
$compact_blocks=1;//checkbox(1) blocks are condensed in one label with all names in it
$lateral_child=0;//checkbox() alternative for blocks display
$liaisons=0;//checkbox() free links solid or dotted between labels
$lateraldecal=15;//integer(15) 
$valign="middle";//select(middle,top,middle) bottom or top or middle (vertical alignment for the lateral relatives) default: middle, bottom if ther are lateral relatives for those who have no sons below
$hook_height=20;//integer(20) height of the hook in the rake (in px)
$vspace=7;//integer(7) vertical space between elements
$sepInBlock=10;//integer(10) // vertical space between blocks element
$Margin=15;//integer(15) // horizontal space between element in a rake
$thickness=1;//integer(1) // 1=2px (thickness of the lines between labels)
$backgroundcolor="transparent";//text() 
$flex=1;//checkbox(1) centered lateral links (bof)

// pdf
$produce_pdf=1;//checkbox() 
$orientation="a4l";//text() a4l (landscape) or a4p (portrait)

// for debugging
$debug=0;//checkbox() 
$aleaLevelForLetters=0;//checkbox() a random level for single lettres: to test disposition of the label0
?>